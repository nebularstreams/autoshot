#!/usr/bin/env bash

# Usage autoshot seriesname filenamepattern
USAGE="Usage: $0 [-s scriptname] SERIESNAME TAKETIME FILENAMEPATTERN STOREROOT"
COPYRIGHT="AutoShot - (C) 2020 Nebular Streams"

# where this resides
RELSCRIPTDIR="$(dirname $(realpath "$0"))"

if [[ "$1" == "-s" ]]; then
  EVENT=$2
  SERIESNAME=$3
  TAKETIME=$4
  FILEPATTERN=$5
  STOREROOT=$6
else
  EVENT=
  SERIESNAME=$1
  TAKETIME=$2
  FILEPATTERN=$3
  STOREROOT=$4
fi

SOFTWARE=$(which screencapture)

if [[ -z ${SOFTWARE} ]]; then
  echo "! This application requires OSX screencapture software or similar to be installed."
  exit 3
fi

if [[ -z "${SERIESNAME}" ]]; then
  echo "${USAGE}"
  exit 1
fi

if [[ -z "${FILEPATTERN}" ]]; then
  FILEPATTERN=Take
fi

if [[ -z "${STOREROOT}" ]]; then
  STOREROOT=/opt/autoshot
fi

if [[ ! -d "${STOREROOT}" ]]; then
  echo "! ${STOREROOT} does not exist, or is not accesible. Please create the folder and rerun the program."
fi

if [[ -z "${TAKETIME}" ]]; then
  TAKETIME=15
fi

echo "${COPYRIGHT}"

echo "- Series Name: ${SERIESNAME}"
echo "- File Pattern: ${FILEPATTERN}"
echo "- Store Root: ${STOREROOT}"
if [[ -n "${EVENT}" ]]; then
  EVENTFILE="${STOREROOT}/scripts/${EVENT}"
  if [[ -f "${EVENTFILE}" ]]; then
    echo "- Found Event Script: ${EVENTFILE}"
  else
    EVENTFILE=${RELSCRIPTDIR}/scripts/${EVENT}
    if [[ -f "${EVENTFILE}" ]]; then
      echo "- Found Provided Event Script: ${EVENTFILE}"
    else
      echo "! Event script ${EVENTFILE} not found. Cannot continue !"
      exit 3
    fi
  fi
fi


if [[ ! -d "${STOREROOT}/series/${SERIESNAME}" ]]; then
  mkdir -p "${STOREROOT}/series/${SERIESNAME}" || {
    echo "! Cannot create ${STOREROOT}/series/${SERIESNAME}, please fix permissions or parameters."
    exit 2
  }
  echo "- Created target folder ${STOREROOT}/series/${SERIESNAME}"
else
  echo "- Target folder ${STOREROOT}/series/${SERIESNAME} already exists. Good."
fi

if [[ ! -d "${STOREROOT}/series/${SERIESNAME}" ]]; then
  mkdir -p "${STOREROOT}/series/${SERIESNAME}" || {
    echo "! Cannot create ${STOREROOT}/series/${SERIESNAME}, please fix permissions or parameters."
    exit 2
  }
  echo "- Created target folder ${STOREROOT}/series/${SERIESNAME}"
else
  echo "- Target folder ${STOREROOT}/series/${SERIESNAME} already exists. Good."
fi

echo -n "👉 Please press ENTER to start the capturing sequence."
read -e
echo ""

while :; do
  FILENAME="${FILEPATTERN}-$(date +%s).jpg"
  echo -n "- Next Take [${FILENAME}] in ${TAKETIME} seconds ..."
  screencapture -m -t jpg -T"${TAKETIME}" "${STOREROOT}/series/${SERIESNAME}/${FILENAME}" || {
    echo -n "❌ "
    exit 1
  }
  echo -n "✅ "
  if [[ -n ${EVENT} ]]; then
    eval "${EVENTFILE}" "${SERIESNAME}" "${STOREROOT}/series/${SERIESNAME}" "${FILENAME}"
    echo -n "⚙️ ︎"
  fi
  echo ""
done
