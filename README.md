# autoshot #

A utility to take series of periodic screenshots, classifying them in folders. At the moment
only works in OSX (because it uses the screencapture program) but can easily be ported to other
platforms just finding a substitute for it. Please contribute !

### Installation ###

Just install the tool on your computer:

```npm install -g git+ssh://git@bitbucket.org/nebularstreams/autoshot.git```

### Usage ###

Just run the program with your desired parameters:

```autoshot [-s scriptname] SERIESNAME [TAKETIME] [FILENAMEPATTERN] [STOREROOT]```

* ```-s scriptname``` executes script in ```[STOREROOT]/[SERIESNAME]/scripts```everytime a new screenshot is captured
* **SERIESNAME** An arbitrary name for the series, example "first_test". This is a required parameter.
* **TAKETIME** Time between captures, in seconds, defaults to 15.
* **FILENAMEPATTERN** Pattern of filenames to be created in the output directory, defaults to "Take"
* **STOREROOT** The root of the data directory, defaults to **```/opt/autoshot```**. Subfolders will be created there for the different series.

The program will start taking screenshots of your MAIN screen and saving them under ```[STOREROOT]/series/[SERIESNAME]/take-xxxxxxxx.jpg```.
 If a script name was specified, it will also run the script with that name, that must have appropriate permissions. Script must
 be placed in ```[STOREROOT]/scripts/[SCRIPTNAME]```.

### Contribution guidelines ###

Contributions welcome.

### License

Creative Commons CC-BY-SA
